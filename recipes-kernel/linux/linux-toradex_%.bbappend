FILESEXTRAPATHS_prepend := "${THISDIR}/linux-toradex:"
 
# Prevent the use of in-tree defconfig
 
CUSTOM_DEVICETREE = "imx6dl-colibri-SCP-v1.dts"
 
SRC_URI += "\ 
	file://${CUSTOM_DEVICETREE} \
	"
 
do_configure_append() {
	# For arm32 bit devices
	cp ${WORKDIR}/${CUSTOM_DEVICETREE} ${S}/arch/arm/boot/dts
	# For arm64 bit freescale/NXP devices
	# cp ${WORKDIR}/${CUSTOM_DEVICETREE} ${S}/arch/arm64/boot/dts/freescale
}
